package th.ac.tu.siit.lab9googlemaps;

import th.ac.tu.siit.lab9googlemaps.R.id;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		map.setMyLocationEnabled(true);
		
		//add marker on the map
		/*MarkerOptions mo = new MarkerOptions();
		mo.position(new LatLng(0,0));
		mo.title("Hello");
		map.addMarker(mo);
		
		
		
		MarkerOptions current = new MarkerOptions();
		//current.position(new LocationManager.GPS_PROVIDER);
		current.position(new LatLng(LocationManager.GPS_PROVIDER));
		current.title("HERE");
		map.addMarker(current);
		
		//add polygon line on the map
		PolylineOptions po = new PolylineOptions();
		po.add(new LatLng(0,0));
		po.add(new LatLng(13.75, 100.4667));
		po.width(5);
		map.addPolyline(po);*/
		
		
		//map.setMyLocationEnabled(true);
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				currentLocation = l;
				
				
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
										//(location, time, distance, method call)
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locListener);
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem i) {
		// TODO Auto-generated method stub
		int p = i.getItemId();
		switch(p)
		{
		case R.id.mark:
			Log.i("AA","AAAAAAA");
			MarkerOptions cur = new MarkerOptions();
			cur.position(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
			cur.title("HEREEEEEEE");
			map.addMarker(cur);
			break;
		}
		
		return super.onOptionsItemSelected(i);
		
		
	}

 
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	

}
